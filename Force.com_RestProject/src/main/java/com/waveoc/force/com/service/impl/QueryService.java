/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waveoc.force.com.service.impl;

import com.waveoc.force.com.domain.Query;
import com.waveoc.force.com.service.IService;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.http.HttpRequest;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.entity.BasicHttpEntity;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.http.HttpHeaders;

/**
 *
 * @author notepad
 */
public class QueryService implements IService {

    
    

    public QueryService() {

    }

    @Override
    public HttpRequest getRequest(Query query) throws UnsupportedEncodingException {
        String paramsString = null;
        String url = "https://ap2.salesforce.com/services/data/v20.0/query/?";
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        for (String value : query.getQueries().values()) {
            params.add(new BasicNameValuePair("q", value));
        }
        paramsString = URLEncodedUtils.format(params, "utf-8");
        url += paramsString;
        HttpGet request = new HttpGet(url);
        request.addHeader(HttpHeaders.AUTHORIZATION, query.getAuthorization());
        System.out.println("URL::::::::" + request.getURI());
        return request;
    }

    @Override
    public HttpRequest getRequest() throws UnsupportedEncodingException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

  
}
