/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waveoc.force.com.domain;

import java.util.Map;

/**
 *
 * @author notepad
 */
public class Query {

    private String accessToken;
    private String tokenType;
    private Map<String, String> queries;

    public Query() {
    }

    public Query(String accessToken, String tokenType, Map<String, String> queries) {
        this.accessToken = accessToken;
        this.tokenType = tokenType;
        this.queries = queries;
    }

    public void setTokenType(String tokenType) {
        this.tokenType = tokenType;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getAuthorization() {
        String authorization = this.tokenType + " " + this.accessToken;
        return authorization;
    }

    public void setQueries(Map<String, String> queries) {
        this.queries = queries;
    }

    public Map<String, String> getQueries() {
        return queries;
    }

    public void addQuery(String key, String value) {
        this.queries.put(key, "SELECT " + value + " FROM " + key + " LIMIT 100");
    }

    public void clearQuery() {
        this.queries.clear();
    }

    @Override
    public String toString() {
        return "tokentype: " + this.tokenType + " token: " + this.accessToken + " queries: " + this.queries.toString();
    }
    
    
}
