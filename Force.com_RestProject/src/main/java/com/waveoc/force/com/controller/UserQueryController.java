/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waveoc.force.com.controller;

import com.waveoc.force.com.domain.Query;
import com.waveoc.force.com.response.ResponseHandler;
import com.waveoc.force.com.service.IService;
import com.waveoc.force.com.service.impl.QueryService;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;
import javax.inject.Inject;
import org.apache.http.HttpRequest;
import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import static org.springframework.web.bind.annotation.RequestMethod.POST;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author notepad
 */
@Controller
public class UserQueryController {

    private final static Logger logger = Logger.getLogger(UserQueryController.class);
    private IService queryService;
    private Query query;

    @Inject
    public UserQueryController(QueryService service, Query query) {
        this.queryService =  service;
        this.query = query;
    }

    @RequestMapping(value = "make/query", method = POST, consumes = "application/json", produces = "application/json")
    @ResponseBody
    public String makeQueryOnForceCom(@CookieValue(value="access_token", defaultValue = "") String accessToken,
            @CookieValue(value="token_type",defaultValue = "") String tokenType,
            @RequestBody String requestQueries) {;
        String parsedResponse = null;
        if (!accessToken.isEmpty() && !tokenType.isEmpty()) {
            this.query.setAccessToken(accessToken);
            this.query.setTokenType(tokenType);
            JSONObject json = new JSONObject(requestQueries);
            for (Object key : json.keySet()) {
                query.addQuery((String) key, json.getString((String) key));
            }
            try {
                HttpRequest request = this.queryService.getRequest(query);
                parsedResponse = ResponseHandler.getResponseContentGET(request);
            } catch (UnsupportedEncodingException ex) {
                logger.error(ex.getMessage());
            } catch (IOException ex) {
                logger.error(ex.getMessage());
            } catch (Exception ex) {
                logger.error(ex.getMessage());
            }
        }
        this.query.clearQuery();
        return parsedResponse;
    }

}
