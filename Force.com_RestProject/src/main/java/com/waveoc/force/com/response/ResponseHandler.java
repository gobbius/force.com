/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waveoc.force.com.response;

import java.io.IOException;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

/**
 *
 * @author notepad
 */
public class ResponseHandler {

    public static String getResponseContentPOST(HttpRequest request) throws IOException {
        HttpClient client = HttpClientBuilder.create().build();
        HttpResponse response = client.execute((HttpPost) request);
        return EntityUtils.toString(response.getEntity());
    }

    public static String getResponseContentGET(HttpRequest request) throws IOException {
        HttpClient client = HttpClientBuilder.create().build();
        HttpResponse response = client.execute((HttpGet) request);
        return EntityUtils.toString(response.getEntity());
    }

}
