/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waveoc.force.com.controller;

import com.waveoc.force.com.response.ResponseHandler;
import com.waveoc.force.com.service.IService;
import com.waveoc.force.com.service.impl.LoginService;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import javax.inject.Inject;
import org.apache.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author notepad
 */
@Controller
public class LoginController {

    private final static Logger logger = Logger.getLogger(LoginController.class);
    private IService loginService;

    @Inject
    public LoginController(LoginService service) {
        this.loginService =  service;
    }

    @RequestMapping(value = "/login", method = GET, produces = "application/json")
    @ResponseBody
    public String loginToForceCom() {
        String parsedResponse = null;
        try {
            HttpRequest request = this.loginService.getRequest();
            parsedResponse = ResponseHandler.getResponseContentPOST(request);
        } catch (UnsupportedEncodingException ex) {
            logger.error(ex.getMessage());
        } catch (IOException ex) {
            logger.error(ex.getMessage());
        }
        return parsedResponse;
    }
}
