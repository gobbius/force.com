/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waveoc.force.com.service;

import com.waveoc.force.com.domain.Query;
import java.io.UnsupportedEncodingException;
import org.apache.http.HttpRequest;

/**
 *
 * @author notepad
 */
public interface IService {
    HttpRequest getRequest() throws UnsupportedEncodingException;
    HttpRequest getRequest(Query query) throws UnsupportedEncodingException;
}
