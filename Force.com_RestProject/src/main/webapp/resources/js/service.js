$(function () {
    $("#loginBtn").click(function () {
        $(this).toggleClass('active');
        $.get("login", function (data) {
            var error = data.hasOwnProperty("error");
            if (data && !error) {
                var now = new Date();
                var time = now.getTime();
                var expireTime = time + 1000 * 1800;
                now.setTime(expireTime);
                $.each(data, function (index, el) {
                    document.cookie = index + "=" + el + ";expires=" + now.toGMTString() + "; path=/";
                });

                var table = $("body").find("#params");
                var children = $(table).children();
                $(children).remove();
                $.each(data, function (index, el) {
                    $(table).append("<tr><td>" + index + "</td><td>" + el + "</td></tr>")
                });
                $("#text").text("Check Cookie");
                $("#text").css("font-weight", "bold")
                $("#objects").find(":first").removeClass("hidden-div");
            } else {
                $("#text").text("Error in connection string, authentication failure");
            }
        }).always(function () {
            $("#loginBtn").toggleClass('active');
        });
    });

    $("#objectBtn").click(function () {
        var object = $("#object").val();
        if (object) {
            $("#query").append("<tr><td>" + object + "</td><td></td><td><i class=\"fa fa-times remove\"></i></td></tr>");
            $("#object").val("");
            registerFunction();
        }
    });

    $("#fieldBtn").click(function () {
        var field = $("#field").val();
        if (field) {
            var object = $("#query").find("tr:last");
            var fieldList = $(object).find("td:nth-child(2)");
            var fields = $(fieldList).text();
            if (fields.length == 0) {
                fields += field;
            } else {
                fields += ", " + field;
            }
            $(fieldList).text(fields);
            $("#field").val("");
        }
    });

    function registerFunction() {
        $(".remove").click(function () {
            var object = $(this).parent().parent();
            $(object).remove();
        });
    }

    $("#make").click(function () {
        var tableData = $("#query").find("tr");
        if (tableData.length) {
            $(this).toggleClass('active');
            var data = "{";
            $.each(tableData, function (index, obj) {
                var object = obj.childNodes[0].textContent;
                var fields = obj.childNodes[1].textContent;
                data += "\"" + object + "\"" + ":" + "\"" + fields + "\",";
            });
            data = data.slice(0, -1);
            data += "}";
            $(".myspinner").removeClass("spinner-hide");
            $.ajax({
                url: "make/query",
                method: "POST",
                contentType: "application/json",
                data: data,
                success: function (data, textStatus, jqXHR) {
                    var error = null;
                    if (data.length != 0) {
                        error = data.hasOwnProperty("0");
                    } else {
                        error = true;
                    }
                    if (data && !error) {
                        var table = $("#queryResult");
                        var records = $(table).children();
                        $(records).remove();
                        $.each(data.records, function (index, obj) {
                            $(table).append("<tr><td>" + JSON.stringify(obj) + "</td></tr>");
                        });
                        $("#text").text("");
                    } else if(data.length != 0) {
                        var errorType = null;
                        var errorMessage = null;
                        if (data[0].hasOwnProperty("errorCode")) {
                            errorType = data[0].errorCode;
                        }

                        if (data[0].hasOwnProperty("message")) {
                            errorMessage = data[0].message;
                        }
                        if (errorType) {
                            $("#text").text(errorType + " => " + errorMessage);
                        }

                    } else {
                         $("#text").text("Cookie is expired");
                    }

                },
                complete: function (jqXHR, textStatus) {
                    $("#make").toggleClass('active');
                }
            });
        }

    });

    $("#object, #field").keyup(function (event) {
        if (event.keyCode == 13) {
            event.preventDefault();
            if ($(this).prop("id") == "object") {
                $("#objectBtn").click();
            } else {
                $("#fieldBtn").click();
            }
        }
    });

});


