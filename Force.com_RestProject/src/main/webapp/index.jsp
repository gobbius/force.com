<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <spring:url value="/resources/js/jquery.min.js" var="jquery" />
        <spring:url value="/resources/js/service.js" var="service" />
        <spring:url value="/resources/css/bootstrap.min.css" var="bootstrap" />
        <spring:url value="/resources/css/styles.css" var="styles" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Login Page</title>
        <script src="${jquery}" type="text/javascript" ></script>
        <script src="${service}" type="text/javascript" ></script>
        <link href="${bootstrap}" rel="stylesheet" />
        <link href="${styles}" rel="stylesheet" />
        <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">
    </head>
    <body>
        <div class="container-fluid">
            <div class="row">
                <div id="objects" class="col-lg-6">
                    <div class="input-group first-row text-center hidden-div">
                        <span class="input-group-btn">
                            <button id="objectBtn" class="btn btn-warning mybtn" type="button">Add</button>
                        </span>
                        <input id="object" type="text" class="form-control" placeholder="Object Name" />
                        <span class="input-group-btn">
                            <button id="fieldBtn" class="btn btn-warning" type="button">Add</button>
                        </span>
                        <input id="field" type="text" class="form-control" placeholder="Field Name" />
                        <span class="input-group-btn">
                            <button id="make" class="btn btn-success login-btn has-spinner make-btn" type="button">
                                <span class="spinner"><i class="fa fa-refresh fa-spin"></i></span>
                                Execute query
                            </button>
                        </span>
                    </div>
                    <table id="query" class="table first-row"></table>
                </div>
                <div class="col-lg-6 text-left">
                    <button id="loginBtn" class="btn btn-success login-btn has-spinner" >
                        <span class="spinner"><i class="fa fa-refresh fa-spin"></i></span>
                        Get token
                    </button>
                    <table id="queryResult" class="table first-row"></table>
                </div>
            </div>

            <div class="row first-row">
                <div id="text" class="col-lg-12 text-center"></div>
            </div>
            <div class="row first-row">
                <div class="col-lg-12">
                    <table id="params" class="table"></table>
                </div>
            </div>

        </div>
    </body>
</html>
